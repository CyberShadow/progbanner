import std.algorithm.comparison;
import std.algorithm.iteration;
import std.algorithm.sorting;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.path;
import std.range;
import std.stdio : toFile, writeln;
import std.typecons;

import ae.utils.aa;
import ae.utils.funopt;
import ae.utils.graphics.color;
import ae.utils.graphics.draw;
import ae.utils.graphics.im_convert;
import ae.utils.graphics.image;
import ae.utils.main;
import ae.utils.math;
import ae.utils.meta;

void draw(string fn, int width, int height)
{
	auto progs = dirEntries("screenshots", SpanMode.shallow)
		.map!(de => de.name)
		.array
		.sort
		.retro
		.map!(fn => fn
			.read
			.parseViaIMConvert!BGR()
			.I!(i => i.crop(9, 1, i.w - 15, i.h - 29))
			.colorMap!(c => c == BGR(38, 26, 24) ? BGRA.black : BGRA(c.b, c.g, c.r, ubyte.max))
			.trim!(c => c != BGRA.black)
			// .I!((i) { i.toBMP.toFile("test-" ~ fn.baseName ~ ".bmp"); return i; })
		)
		.array;

	auto numProgs = progs.length.to!int;
	enforce(numProgs > 1, "Not enough program screenshots");

	auto bgs = numProgs.iota.map!(n => bg(float(n) / (numProgs - 1))).retro.array;

	// int fragSize = (banner.w + banner.h) / numProgs.to!int + 1;

	// Program (centered on y=0)
	BGRA progPixel(size_t p, int x, int y)
	{
		auto prog = progs[p];
		y += prog.h / 2;
		if (x < 0 || y < 0 || x >= prog.w || y >= prog.h || prog[x, y] == BGRA.black)
		{
			auto bg = bgs[p];
			return BGRA(bg.b, bg.g, bg.r, ubyte.max);
		}
		else
			return prog[x, y];
	}

	// How many extra pixels (away from center) to include from first/last programs.
	enum collagePadding = 55;

	// Vertical distance from center of first program to center of last program
	// Bottom-left corner should be around the center of first program;
	// top-right corner should be around the center of last program.
	auto collageSize = (width + height) / 2 - 2 * collagePadding;

	// The gradient is thus split up into this many non-solid sections.
	auto numSections = numProgs - 1;

	// Unrotated collage
	BGRA collagePixel(int x, int y)
	{
		y -= collagePadding;

		auto idx = numSections * y / collageSize;
		int[2] progIndex = [idx, idx + 1];
		foreach (ref p; progIndex)
			p = p.bound(0, numSections);

		auto y0 = collageSize * progIndex[0] / numSections;
		auto y1 = collageSize * progIndex[1] / numSections;

		BGRA[2] cs;
		foreach (i; 0..2)
		{
			auto px = x + (collageSize * progIndex[i] / numSections);
			auto py = y - (collageSize * progIndex[i] / numSections);

			// Shift terminal programs into view
			if (progIndex[i] == 0)
				px += (height - 400) / 3;
			if (progIndex[i] == numSections)
				px -= (height - 400);

			cs[i] = progPixel(progIndex[i], px, py);
		}
		if (y0 == y1 || y < 0)
			return cs[0];
		else
			return BGRA.itpl(cs[0], cs[1], y, y0, y1);
	}

	void rotCoord(ref int x, ref int y)
	{
		static int euclideanDivide(int a, int b)
		{
			enum ofs = 0x4000_0000;
			return (ofs + a) / b - (ofs / b);
		}

		x -= width;
		auto ix = euclideanDivide(x + y,  2);
		auto iy = euclideanDivide(y - x,  2);

		// Account for collagePadding shift and move line start of
		// program center back at x=0
		ix += collagePadding; 

		// Shift things a bit back towards the center for wide (tall) banners
		ix -= (height - 400) / 3;

		x = ix;
		y = iy;
	}

	// Rotated collage
	BGRA rotPixel(int x, int y)
	{
		rotCoord(x, y);
		return collagePixel(x, y);
	}

	procedural!rotPixel(width, height).toBMP.toFile(fn);

	auto frame =
		chain(
			(width  + 1).iota.map!(x => only(tuple(x, -1), tuple(x, height))).joiner,
			(height + 1).iota.map!(y => only(tuple(-1, y), tuple(width , y))).joiner,
		)
		.map!((c) { rotCoord(c[0], c[1]); return c; })
		.toSet;

	auto dbg = procedural!(
		(x, y)
		{
			x -= collageSize;
			y -= 100;
			if (y == collagePadding || y == collagePadding + collageSize)
				return BGRA(0xFF, 0, 0, 0xFF);
			if (x == 0 || y == 0)
				return BGRA(0, 0, 0xFF, 0xFF);
			if (tuple(x, y) in frame)
				return BGRA(0, 0xFF, 0, 0xFF);
			return collagePixel(x, y);
		}
	)(collageSize * 2, collageSize + 400).copy;


	dbg.toBMP.toFile((fn.stripExtension ~ "_debug").setExtension(fn.extension));
}

mixin main!(funopt!draw);

RGB bg(float f)
{
	enum l = 0x20;
	enum L = l*3/2;

	static immutable RGB[] colors =
		[
			RGB(0, 0, L),
			RGB(0, l, l),
			RGB(0, L, 0),
			RGB(l, l, 0),
			RGB(L, 0, 0),
			RGB(l, 0, l),
		];
	alias G = Gradient!(float, RGB);
	import std.range : iota;
	import std.array : array;
	static immutable grad = G((colors.length+1).iota.map!(n => G.Point(1f / colors.length * n, colors[n % $])).array);

	return grad.get(f);
}
