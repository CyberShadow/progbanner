ParseOptions parseOptions;
parseOptions.stripComments = stripComments;
parseOptions.mode = obfuscate ? ParseOptions.Mode.words : ParseOptions.Mode.source;
parseOptions.rules = splitRules.map!parseSplitRule().array();
measure!"load"({root = loadFiles(dir, parseOptions);});
enforce(root.children.length, "No files in specified directory");

applyNoRemoveMagic();
applyNoRemoveRegex(noRemoveStr, reduceOnly);
applyNoRemoveDeps();
if (coverageDir)
	loadCoverage(coverageDir);
if (!obfuscate && !noOptimize)
	optimize(root);
maxBreadth = getMaxBreadth(root);
countDescendants(root);
resetProgress();
assignID(root);
//
