(defun term-keys/iterate-keys (fun)
  "Call FUN over every enabled key combination.

Iterate over all elements of `term-keys/mapping' and modifier key
combinations, filter the enabled ones using
`term-keys/want-key-p-func', and call (FUN INDEX KEYMAP MODS),
where INDEX is the key index in `term-keys/mapping', KEYMAP is
the `term-keys/mapping' element vector at that index, and MODS is
a bool vector for the active modifier keys.

Collect FUN's return values in a list and return it."
  (cl-loop
   for keymap in term-keys/mapping
   for index from 0
   append
   (cl-loop
    ;; Iterate from 0 to 2^64-1 for a bitmask of all modifier combinations
    for modnum from 0 to (1- (lsh 1 (length term-keys/modifier-chars)))
    ;; Convert the integer bitmask to a bool-vector
    for mods = (apply #'bool-vector (mapcar (lambda (n) (not (zerop (logand modnum (lsh 1 n)))))
					    (number-sequence 0 (1- (length term-keys/modifier-chars)))))
    if (and
	(elt keymap 0)                  ; Representable in Emacs?
	(funcall term-keys/want-key-p-func (elt keymap 1) mods)) ; Want this key combination?
    collect (funcall fun index keymap mods))))


;;;###autoload
(defun term-keys/init ()
  "Set up configured key sequences for the current terminal."
  (interactive)
  (term-keys/iterate-keys
   (lambda (index keymap mods)
     (define-key
       input-decode-map
       (concat
	term-keys/prefix
	(term-keys/encode-key index mods)
	term-keys/suffix)
       (kbd (term-keys/format-key
	     (elt keymap 0) mods))))))
