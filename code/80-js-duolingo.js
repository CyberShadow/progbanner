for (var i = 0; i < buttons.length; i++) {
  var b = buttons[i];
  if (b.classList.contains(classNameDisabled))
    continue;

  var div = b.querySelector('div.key-hint');
  if (!div) {
    var text = b.textContent;
    if (text in usedWords) {
      var key = usedWords[text];
      div = document.createElement('div');
      div.classList.add('key-hint');
      b.insertBefore(div, b.firstChild);
      div.textContent = key;
      currentButtons[key] = b;
    }
  } else {
    var key = b.childNodes[0].textContent;
    currentButtons[key] = b;
  }
}
