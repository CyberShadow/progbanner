#!/bin/bash
if [[ ${#unknown_foreign_packages[@]} != 0 ]]
then
	LogEnter 'Found %s unknown foreign packages. Registering...\n' "$(Color G ${#unknown_foreign_packages[@]})"
	printf '\n\n# %s - Unknown foreign packages\n\n\n' "$(date)" >> "$config_save_target"
	for package in "${unknown_foreign_packages[@]}"
	do
		Log '%s...\r' "$(Color M "%q" "$package")"
		local description
		description="$(LC_ALL=C "$PACMAN" --query --info "$package" | grep '^Description' | cut -d ':' -f 2)"
		printf 'AddPackage --foreign %q #%s\n' "$package" "$description" >> "$config_save_target"
	done
	modified=y
	LogLeave
fi
                                                                                                          #
                                                                                                          #
