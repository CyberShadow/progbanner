tries = 0; // Iterations without any improvement
totalTries = saTriesLimit + saCoolTries;

e (tries < totalTries)

float time = (float)tries / (float)saTriesLimit; // Temperature and mutation is 0 when time>1
float temperature = time > 1 ? 0 : (float)Math.Pow(1 - time, saCoolExp);
Log("Try " + tries + "/" + saTriesLimit + "+" + saCoolTries + ", temperature " + temperature);
float mutationRate = time > 1 ? 0 : (float)Math.Pow(1 - time, saMutExp);
Solution currentSolution = goodSolution.Mutate(mutationRate, mutationRate);

Log("Current best: " + bestSolution.ToString());
Log("Current good: " + goodSolution.ToString());
Log("Candidate   : " + currentSolution.ToString());
yield return currentSolution;
