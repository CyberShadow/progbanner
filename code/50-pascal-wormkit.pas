                                                                                                                       {}
                                                                                                                       {}
if Length(Arr)<>3 then
  continue;

ExternalSocket := true;
GamePort := GetProfileInt('NetSettings', 'HostingPort', 17011);

ProcessHandle := OpenProcess(PROCESS_DUP_HANDLE, FALSE, StrToInt(Arr[0]));
if ProcessHandle<>0 then
begin
  DuplicateHandle(ProcessHandle, THandle(StrToInt(Arr[1])), GetCurrentProcess(), @ControlSocket, 0, FALSE, DUPLICATE_SAME_ACCESS or DUPLICATE_CLOSE_SOURCE);
  DuplicateHandle(ProcessHandle, THandle(StrToInt(Arr[2])), GetCurrentProcess(), @Event        , 0, FALSE, DUPLICATE_SAME_ACCESS);
  CloseHandle(ProcessHandle);
  Log('New socket: ' + IntToStr(ControlSocket));
                                                                                                                       {}
                                                                                                                       {}
