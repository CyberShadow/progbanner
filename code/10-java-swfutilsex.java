				  for (int i = 0; i < n; i++)
				  {
					  int start = offset;
					  MethodInfo m = methods[i] = new MethodInfo();
					  m.paramCount = (int) readU32();
					  m.returnType = (int) readU32();
					  m.params = new int[m.paramCount];
					  for (int j = 0; j < m.paramCount; j++)
					  {
						  m.params[j] = (int) readU32();
					  }
