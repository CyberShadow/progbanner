HRESULT hr;

// Set vertex shader
hr = d3dDevice->SetVertexShader(NULL);
CHECK_ERROR(hr, "SetVertexShader");

hr = d3dDevice->SetFVF(D3DFVF_TLVERTEX);
CHECK_ERROR(hr, "SetFVF");

// Setup rendering states
hr = d3dDevice->SetRenderState(D3DRS_LIGHTING, FALSE);
CHECK_ERROR(hr, "SetRenderState");

// Create vertex buffer and set as stream source

hr = d3dDevice->CreateVertexBuffer(sizeof(TLVERTEX) * 4, NULL, D3DFVF_TLVERTEX, D3DPOOL_MANAGED,
	&vertexBuffer, NULL);
CHECK_ERROR(hr, "CreateVertexBuffer");
hr = d3dDevice->SetStreamSource(0, vertexBuffer, 0, sizeof(TLVERTEX));
CHECK_ERROR(hr, "SetStreamSource");

RECT rDest = {0, 0, m_iWidth, m_iHeight};
TLVERTEX* vertices;

hr = vertexBuffer->Lock(0, 0, (void **)&vertices, NULL);
CHECK_ERROR(hr, "IDirect3DVertexBuffer9::Lock");

vertices[0].x = (float) rDest.left - 0.5f;
vertices[0].y = (float) rDest.top - 0.5f;
vertices[0].z = 0.0f;
vertices[0].rhw = 1.0f;
vertices[0].u = 0.0f;
vertices[0].v = 0.0f;
