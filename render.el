(progn
  (remove-hook 'prog-mode-hook 'highlight-thing-mode)
  (add-hook 'prog-mode-hook
	    (lambda ()
	      (flycheck-mode 0)
	      (when (eq major-mode 'js2-mode)
		(js-mode))
	      (run-at-time "0.5 second" nil
			   (lambda ()
			     (goto-char (point-max))))
	      (run-at-time "1 second" nil
			   (lambda ()
			     (call-process
			      "scrot" nil nil nil "--focused"
			      (concat
			       "../screenshots/"
			       (file-name-nondirectory buffer-file-name)
			       ".png")
			      )
			     (kill-emacs)
			     ))
	      )
	    )
  )
